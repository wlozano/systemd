Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: GPL-2 or LGPL-2.1

Files: .ycm_extra_conf.py
Copyright: interest in the
License: Unlicense

Files: debian/*
Copyright: 2010-2013 Tollef Fog Heen <tfheen@debian.org>
           2013-2018 Michael Biebl <biebl@debian.org>
           2013 Michael Stapelberg <stapelberg@debian.org>
License: LGPL-2.1+

Files: debian/extra/systemd.py
 debian/extra/udev.py
Copyright: 2009, 2014-2016, Canonical Ltd.
License: LGPL-2.1+

Files: debian/extra/tmpfiles.d/*
Copyright: 2010-2013 Tollef Fog Heen <tfheen@debian.org>
           2013-2018 Michael Biebl <biebl@debian.org>
           2013 Michael Stapelberg <stapelberg@debian.org>
License: GPL-2+

Files: debian/lib.systemd.systemd-logind
Copyright: 2015-2017, Collabora Ltd.
License: MPL-2.0

Files: debian/tests/boot-and-services
 debian/tests/build-login
 debian/tests/storage
 debian/tests/systemd-fsckd
 debian/tests/udev
 debian/tests/unit-config
Copyright: 2009, 2014-2016, Canonical Ltd.
License: LGPL-2.1+

Files: hwdb.d/*
Copyright: no-info-found
License: Expat

Files: man/systemd-fsckd.service.xml
Copyright: 2015, Canonical
License: LGPL-2.1+

Files: modprobe.d/*
Copyright: no-info-found
License: LGPL-2.1+

Files: network/*
Copyright: no-info-found
License: LGPL-2.1+

Files: presets/*
Copyright: no-info-found
License: LGPL-2.1+

Files: rules.d/*
Copyright: no-info-found
License: LGPL-2.1+

Files: shell-completion/*
Copyright: no-info-found
License: LGPL-2.1+

Files: shell-completion/bash/coredumpctl
 shell-completion/bash/hostnamectl
 shell-completion/bash/journalctl
 shell-completion/bash/localectl
 shell-completion/bash/loginctl
 shell-completion/bash/systemd-analyze
 shell-completion/bash/timedatectl
 shell-completion/bash/udevadm
Copyright: 2010, Ran Benita
License: LGPL-2.1+

Files: src/*
Copyright: no-info-found
License: LGPL-2.1+

Files: src/basic/*
Copyright: no-info-found
License: public-domain

Files: src/basic/linux/*
Copyright: no-info-found
License: GPL-2+

Files: src/basic/linux/btrfs.h
Copyright: 2007, Oracle.
License: GPL-2

Files: src/basic/linux/if.h
Copyright: UCB 1982-1988
License: GPL-2+

Files: src/basic/linux/if_arp.h
Copyright: UCB 1986-1988
License: GPL-2+

Files: src/basic/linux/if_macsec.h
Copyright: 2015, Sabrina Dubroca <sd@queasysnail.net>
License: GPL-2+

Files: src/basic/linux/if_tun.h
Copyright: 1999, 2000, Maxim Krasnyansky <max_mk@yahoo.com>
License: GPL-2+

Files: src/basic/sparse-endian.h
Copyright: 2012, Josh Triplett <josh@joshtriplett.org>
License: Expat

Files: src/basic/utf8.c
Copyright: 2000, Red Hat, Inc.
 1999, Tom Tromey
License: LGPL-2+

Files: src/boot/efi/sha256.c
Copyright: blurb: / 2007-2019, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: src/fsckd/*
Copyright: 2015, Canonical
License: LGPL-2.1+

Files: src/journal/*
Copyright: 2012, B. Poettering
License: LGPL-2.1+

Files: src/journal/journald.conf
Copyright: no-info-found
License: LGPL-2.1+

Files: src/journal/lookup3.c
Copyright: no-info-found
License: public-domain

Files: src/network/networkd.conf
 src/network/org.freedesktop.network1.conf
 src/network/org.freedesktop.network1.policy
 src/network/org.freedesktop.network1.service
Copyright: no-info-found
License: LGPL-2.1+

Files: src/shared/*
Copyright: 1995-2004, Miquel van Smoorenburg
License: LGPL-2+

Files: src/shared/linux/*
Copyright: 2008, Red Hat, Inc.
 2008, Ian Kent <raven@themaw.net>
License: GPL-2

Files: src/shared/linux/bpf.h
Copyright: 2011-2014, PLUMgrid, http:plumgrid.com
License: GPL-2

Files: src/shared/linux/dm-ioctl.h
Copyright: 2004-2009, Red Hat, Inc.
 2001-2003, Sistina Software (UK) Limited.
License: LGPL

Files: src/shared/linux/nl80211.h
Copyright: 2018, 2019, Intel Corporation
 2015-2017, Intel Deutschland GmbH
 2008, Michael Wu <flamingice@sourmilk.net>
 2008, Michael Buesch <m@bues.ch>
 2008, Luis Carlos Cobo <luisca@cozybit.com>
 2008, Jouni Malinen <jouni.malinen@atheros.com>
 2008, Colin McCabe <colin@cozybit.com>
 2008, 2009, Luis R. Rodriguez <lrodriguez@atheros.com>
 2006-2010, Johannes Berg <johannes@sipsolutions.net>
License: ISC

Files: src/systemd/sd-dhcp-client.h
 src/systemd/sd-dhcp-lease.h
 src/systemd/sd-dhcp-option.h
 src/systemd/sd-dhcp-server.h
 src/systemd/sd-dhcp6-client.h
 src/systemd/sd-dhcp6-lease.h
 src/systemd/sd-ndisc.h
 src/systemd/sd-radv.h
Copyright: 2013-2015, 2017, Intel Corporation.
License: LGPL-2.1+

Files: src/systemd/sd-ipv4acd.h
 src/systemd/sd-ipv4ll.h
Copyright: 2014, Axis Communications AB.
License: LGPL-2.1+

Files: src/test/test-systemd-tmpfiles.py
Copyright: no-info-found
License: LGPL-2.1+

Files: src/udev/mtd_probe/*
Copyright: 2010, - Maxim Levitsky
License: GPL-2+

Files: src/udev/scsi_id/scsi.h
Copyright: IBM Corp. 2003
License: GPL-2

Files: src/udev/udev-ctrl.c
Copyright: no-info-found
License: LGPL-2.1+

Files: src/udev/udevadm-control.c
Copyright: no-info-found
License: GPL-2+

Files: src/udev/v4l_id/*
Copyright: 2009, Filippo Argiolas <filippo.argiolas@gmail.com>
License: GPL-2+

Files: src/vconsole/90-vconsole.rules.in
Copyright: no-info-found
License: LGPL-2.1+

Files: sysctl.d/*
Copyright: no-info-found
License: LGPL-2.1+

Files: sysusers.d/*
Copyright: no-info-found
License: LGPL-2.1+

Files: test/fuzz/*
Copyright: no-info-found
License: LGPL-2.1+

Files: test/test-path/*
Copyright: no-info-found
License: LGPL-2.1+

Files: test/units/*
Copyright: no-info-found
License: LGPL-2.1+

Files: tmpfiles.d/*
Copyright: no-info-found
License: LGPL-2.1+

Files: tools/*
Copyright: 2014, The Chromium OS Authors.
License: BSD-3-clause

Files: tools/catalog-report.py
Copyright: no-info-found
License: Expat

Files: units/*
Copyright: no-info-found
License: LGPL-2.1+

Files: .mkosi/* catalog/* catalog/systemd.bg.catalog.in catalog/systemd.fr.catalog.in catalog/systemd.hu.catalog.in catalog/systemd.it.catalog.in catalog/systemd.pt_BR.catalog.in catalog/systemd.zh_CN.catalog.in catalog/systemd.zh_TW.catalog.in docs/* docs/fonts/* hwdb.d/pci.ids man/* man/environment.d.xml man/journal-remote.conf.xml man/networkd.conf.xml man/standard-conf.xml man/systemd-machine-id-commit.service.xml man/tmpfiles.d.xml man/udev.xml shell-completion/bash/systemctl.in src/analyze/* src/basic/gunicode.c src/basic/gunicode.h src/basic/gunicode.c src/basic/gunicode.h src/basic/raw-clone.h src/basic/smack-util.c src/basic/smack-util.h src/basic/smack-util.c src/basic/smack-util.h src/boot/* src/boot/efi/crc32.c src/boot/efi/shim.c src/boot/efi/shim.h src/boot/efi/shim.c src/boot/efi/shim.h src/core/dbus-swap.c src/core/dbus-swap.h src/core/swap.h src/core/dbus-swap.c src/core/dbus-swap.h src/core/swap.h src/core/dbus-swap.c src/core/dbus-swap.h src/core/swap.h src/core/ima-setup.c src/core/ima-setup.h src/core/ima-setup.c src/core/ima-setup.h src/core/killall.c src/core/load-fragment.c src/core/namespace.h src/core/smack-setup.c src/core/smack-setup.h src/core/smack-setup.c src/core/smack-setup.h src/core/triggers.systemd.in src/fsck/* src/import/import-pubring.gpg src/libsystemd-network/* src/libsystemd-network/arp-util.c src/libsystemd-network/arp-util.h src/libsystemd-network/sd-ipv4acd.c src/libsystemd-network/sd-ipv4ll.c src/libsystemd-network/test-ipv4ll.c src/libsystemd-network/arp-util.c src/libsystemd-network/arp-util.h src/libsystemd-network/sd-ipv4acd.c src/libsystemd-network/sd-ipv4ll.c src/libsystemd-network/test-ipv4ll.c src/libsystemd-network/arp-util.c src/libsystemd-network/arp-util.h src/libsystemd-network/sd-ipv4acd.c src/libsystemd-network/sd-ipv4ll.c src/libsystemd-network/test-ipv4ll.c src/libsystemd-network/arp-util.c src/libsystemd-network/arp-util.h src/libsystemd-network/sd-ipv4acd.c src/libsystemd-network/sd-ipv4ll.c src/libsystemd-network/test-ipv4ll.c src/libsystemd-network/arp-util.c src/libsystemd-network/arp-util.h src/libsystemd-network/sd-ipv4acd.c src/libsystemd-network/sd-ipv4ll.c src/libsystemd-network/test-ipv4ll.c src/libsystemd/sd-hwdb/* src/libudev/libudev-queue.c src/network/* src/network/netdev/* src/network/netdev/wireguard.c src/network/networkd-brvlan.c src/network/networkd-brvlan.h src/network/networkd-brvlan.c src/network/networkd-brvlan.h src/network/networkd-conf.c src/network/networkd-conf.h src/network/networkd-conf.c src/network/networkd-conf.h src/network/networkd-nexthop.c src/network/networkd-nexthop.h src/network/networkd-sriov.c src/network/networkd-sriov.h src/network/networkd-nexthop.c src/network/networkd-nexthop.h src/network/networkd-sriov.c src/network/networkd-sriov.h src/network/networkd-nexthop.c src/network/networkd-nexthop.h src/network/networkd-sriov.c src/network/networkd-sriov.h src/network/networkd-nexthop.c src/network/networkd-nexthop.h src/network/networkd-sriov.c src/network/networkd-sriov.h src/network/tc/* src/pstore/pstore.c src/shared/sleep-config.c src/shutdown/* src/sleep/sleep.c src/sulogin-shell/* src/test/* src/test/test-conf-files.c src/test/test-list.c src/test/test-sched-prio.c src/test/test-udev.c src/tty-ask-password-agent/* src/udev/* src/udev/ata_id/* src/vconsole/* test/* test/fuzz/fuzz-dhcp6-client/* test/sd-script.py test/test-exec-deserialization.py test/test-resolve/* test/test-resolve/kyhwana.org.pkts test/test-resolve/org~20200417.pkts test/test-resolve/root.pkts test/test-resolve/vdwaa.nl~20200417.pkts test/test-resolve/zbyszek@fedoraproject.org.pkts test/udev-test.pl tools/chromiumos/gen_autosuspend_rules.py
Copyright: 2008-2015 Kay Sievers <kay@vrfy.org>
 2010-2015 Lennart Poettering
 2012-2015 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl>
 2013-2015 Tom Gundersen <teg@jklm.no>
 2013-2015 Daniel Mack
 2010-2015 Harald Hoyer
 2013-2015 David Herrmann
 2013, 2014 Thomas H.P. Andersen
 2013, 2014 Daniel Buch
 2014 Susant Sahani
 2009-2015 Intel Corporation
 2000, 2005 Red Hat, Inc.
 2009 Alan Jenkins <alan-jenkins@tuffmail.co.uk>
 2010 ProFUSION embedded systems
 2010 Maarten Lankhorst
 1995-2004 Miquel van Smoorenburg
 1999 Tom Tromey
 2011 Michal Schmidt
 2012 B. Poettering
 2012 Holger Hans Peter Freyther
 2012 Dan Walsh
 2012 Roberto Sassu
 2013 David Strauss
 2013 Marius Vollmer
 2013 Jan Janssen
 2013 Simon Peeters
License: LGPL-2.1+

Files: src/basic/linux/can/* src/basic/linux/if_bonding.h src/basic/linux/libc-compat.h src/basic/linux/wireguard.h src/shared/linux/ethtool.h
Copyright: 2004-2009 Red Hat, Inc.
 2011-2014 PLUMgrid
 2001-2003 Sistina Software (UK) Limited.
 2008 Ian Kent <raven@themaw.net>
 1998 David S. Miller >davem@redhat.com>
 2001 Jeff Garzik <jgarzik@pobox.com>
 2006-2010 Johannes Berg <johannes@sipsolutions.net
 2008 Michael Wu <flamingice@sourmilk.net>
 2008 Luis Carlos Cobo <luisca@cozybit.com>
 2008 Michael Buesch <m@bues.ch>
 2008, 2009 Luis R. Rodriguez <lrodriguez@atheros.com>
 2008 Jouni Malinen <jouni.malinen@atheros.com>
 2008 Colin McCabe <colin@cozybit.com>
 2018-2019 Intel Corporation
 2007 Oracle.
 2009 Wolfgang Grandegger <wg@grandegger.com>
 1999 Thomas Davis <tadavis@lbl.gov>
 2015 Sabrina Dubroca <sd@queasysnail.net>
 1999-2000 Maxim Krasnyansky <max_mk@yahoo.com>
 2015-2019 Jason A. Donenfeld <Jason@zx2c4.com>
License: GPL-2 with Linux-syscall-note exception

Files: src/udev/scsi_id/* src/udev/scsi_id/scsi_id.c
Copyright: 2003 IBM Corp.
License: GPL-2+

Files: src/udev/udev-builtin-blkid.c src/udev/udev-builtin-input_id.c src/udev/udev-builtin-kmod.c src/udev/udev-builtin-usb_id.c src/udev/udev-event.h src/udev/udevadm-test.c src/udev/udev-event.h src/udev/udevadm-test.c src/udev/udevd.c
Copyright: 2003-2012 Kay Sievers <kay@vrfy.org>
 2003-2004 Greg Kroah-Hartman <greg@kroah.com>
 2004 Chris Friesen <chris_friesen@sympatico.ca>
 2004, 2009, 2010 David Zeuthen <david@fubar.dk>
 2005, 2006 SUSE Linux Products GmbH
 2003 IBM Corp.
 2007 Hannes Reinecke <hare@suse.de>
 2009 Canonical Ltd.
 2009 Scott James Remnant <scott@netsplit.com>
 2009 Martin Pitt <martin.pitt@ubuntu.com>
 2009 Piter Punk <piterpunk@slackware.com>
 2009, 2010 Lennart Poettering
 2009 Filippo Argiolas <filippo.argiolas@gmail.com>
 2010 Maxim Levitsky
 2011 ProFUSION embedded systems
 2011 Karel Zak <kzak@redhat.com>
 2014 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl>
 2014 David Herrmann <dh.herrmann@gmail.com>
 2014 Carlos Garnacho <carlosg@gnome.org>
License: GPL-2+
